FROM java:8-jre
MAINTAINER Synapse <synapse.talk@gmail.com>

VOLUME /tmp
ADD ./sy-devmonitor.jar /app/sy-devmonitor.jar
ADD ./application.properties /app/
CMD ["java", "-Dspring.config.location=/app/application.properties", "-jar", "/app/sy-devmonitor.jar"]

EXPOSE 8080