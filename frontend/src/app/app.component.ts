import { Component } from '@angular/core';
import {SyNavbarService} from "./services/navbar.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  sidenavOpened: boolean;

  constructor(private navbar: SyNavbarService) {
    navbar.stateChange.subscribe(v => this.sidenavOpened = v);
  }

  clickMenu() {
    this.navbar.toggle();
  }
}
