'use strict';

/**
 * @ngdoc overview
 * @name syClientangularApp
 * @description
 * # syClientangularApp
 *
 * Main module of the application.
 */
angular
  .module('syClientangularApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    //'ui.bootstrap',
    'ngMaterial',
    'ngMdIcons',
    'md.data.table'
  ])
  .config(function ($routeProvider, $mdThemingProvider, $mdIconProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('pink')
      .accentPalette('orange');

    $routeProvider
      /*.when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })*/
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/about'
      });


  })
  .constant("settings", {
    "webSocketURL": 'ws://domain.com/websocket',
    "apiUrlDevMonitor": 'http://192.168.3.10:28080/api/v1/devmonitor',
    "webSocketTopic": "name",
    "appName": "Synapse"
  });
