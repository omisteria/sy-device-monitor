import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import { AppComponent } from './app.component';
import {RouterModule, Routes} from "@angular/router";
import {SharedModule} from "./modules/shared.module";
import {ApiService} from "./services/api.service";
import {HomeModule} from "./views/home/home.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SyDeviceDetailsComponent} from "./comp/device-details/device-details.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {MaterialModule} from "./modules/material.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

export const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home/landing'
  },
  {
    path     : 'home',
    loadChildren: './views/home/home.module#HomeModule'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    SyDeviceDetailsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    SharedModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    HomeModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ApiService,
    {provide: MatDialogRef, useValue: {}},
    {provide: MAT_DIALOG_DATA, useValue: {}}
  ],
  entryComponents: [
    SyDeviceDetailsComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
