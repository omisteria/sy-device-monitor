import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from "@angular/material";
import {Device, DeviceObserver} from "../../models/device";
import {ApiService} from "../../services/api.service";
import {Observable} from "rxjs/Rx";

@Component({
  selector: 'sy-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.scss']
})
export class SyDeviceDetailsComponent {

  formGroup: FormGroup;
  availablePlatforms: Observable<any>;
  device: Device;

  constructor(
    public fb: FormBuilder,
    private snackBar: MatSnackBar,
    private apiService: ApiService,
    public dialogRef: MatDialogRef<SyDeviceDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

    this.device = this.data.row;

    if (this.device) {
      this.formGroup = fb.group({
        'id': [this.device.deviceId, [Validators.required, Validators.minLength(4)]],
        'description': [this.device.description, [Validators.required, Validators.minLength(4)]],
        'ipAddress': [this.device.ipAddress, [Validators.required, Validators.minLength(11)]],
        'mac': [this.device.mac, [Validators.required, Validators.minLength(11)]],
        'platform': [this.device.type]
      });
    } else {
      this.device = new Device();
      this.formGroup = fb.group({
        'id': ['', [Validators.required, Validators.minLength(4)]],
        'description': ['', [Validators.required, Validators.minLength(4)]],
        'ipAddress': ['', [Validators.required, Validators.minLength(11)]],
        'mac': ['', [Validators.required, Validators.minLength(11)]],
        'platform': ['android']
      });
    }

    this.availablePlatforms = this.apiService.arrayPlatforms();

  }

  save() {
    if (this.formGroup.valid) {

      const isNew = !!this.device;
      if (!isNew) {

      }
      this.device.deviceId = this.formGroup.get('id').value;
      this.device.description = this.formGroup.get('description').value;
      this.device.ipAddress = this.formGroup.get('ipAddress').value;
      this.device.mac = this.formGroup.get('mac').value;
      this.device.type = this.formGroup.get('platform').value;

      this.apiService.saveDevice(this.device)
        .subscribe(data => {
          this.dialogRef.close(data);
        });
    }
  }

  discard() {
    this.dialogRef.close();
  }
}
