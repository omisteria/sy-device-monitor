import {Component, Input} from '@angular/core';

@Component({
  selector: 'sy-platform',
  templateUrl: './platform.component.html',
  styleUrls: ['./platform.component.scss']
})
export class SyPlatformComponent {

  @Input()
  platform: string;

  @Input()
  status: boolean;

  getClass() {
    if (this.status) {
      return 'status-active';
    } else {
      return 'status-inactive';
    }
  }

  getTooltipText() {
    if (this.status) {
      return 'ACTIVE';
    } else {
      return 'NOT AVAILABLE';
    }
  }
}
