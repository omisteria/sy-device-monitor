'use strict';

/**
 * @ngdoc function
 * @name syClientangularApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the syClientangularApp
 */
angular.module('syClientangularApp')
  .controller('AboutCtrl', function ($scope, $mdDialog, DevMonitorService) {

    $scope.vm = {
      deviceObservers: [],
      tableMeta : {
        order: 'deviceId',
        limit: 30,
        page: 1
      },
      selected : []
    };



    var stompClient;
    var attempts = 1;
    var socket;

    function generateInteval (k) {
      return Math.min(30, (Math.pow(2, k) - 1)) * 1000;
    }

    function createWebSocket() {
      socket = new SockJS('http://192.168.3.10:28080/gs-guide-websocket');
      stompClient = Stomp.over(socket);
      stompClient.connect({}, onWSConnect, onWSClose);
    };

    function onWSConnect(frame) {
      attempts = 1;

      //console.log('Connected: ' + frame);

      stompClient.subscribe('/topic/deviceObserver', function (result) {
        var observer = JSON.parse(result.body);
        if (observer) {
          var dev = _.filter($scope.vm.deviceObservers, function (item) {
            return item.deviceId == observer.deviceId;
          })[0];
          if (dev) {
            dev.available = observer.available;
            dev.timeStatusChange = observer.timeStatusChange;
          } else {
            $scope.vm.deviceObservers.push(observer);
          }
          $scope.$apply();
        }
      });
    };

    function onWSClose() {
      var time = generateInteval(attempts);

      setTimeout(function () {
        attempts++;
        createWebSocket();
      }, time);
    };


    createWebSocket();

    $scope.getObservers = function () {
      //$scope.promise = $nutrition.desserts.get($scope.query, success).$promise;
      $scope.promise = DevMonitorService.getAllDevices().then(function(list) {
        $scope.vm.deviceObservers = list;
        //angular.forEach($scope.vm.deviceObservers, function(value, key) {
        //});
      });
    };

    $scope.getObservers();


    $scope.showAdd = function(ev) {
      $mdDialog.show({
        controller: 'DeviceObserverDialogController',
        templateUrl: 'templates/device-observer.html',
        targetEvent: ev,
        locals: {
          platforms: DevMonitorService.arrayPlatforms(),
          observer: null
        }
      })
        .then(
          function(answer) {
            DevMonitorService.addDevice({data:answer}).then(
              function(methodAnswer) {
                $scope.vm.deviceObservers.push(answer);
              },
              function(methodAnswer) {

              }
            );
          }, function() {

          }
        );
    };

    $scope.doEditObserver = function(ev, observer) {
      $mdDialog.show({
        controller: 'DeviceObserverDialogController',
        templateUrl: 'templates/device-observer.html',
        targetEvent: ev,
        locals: {
          platforms: DevMonitorService.arrayPlatforms(),
          observer: observer
        }
      })
        .then(
          function(answer) {
            DevMonitorService.updateDevice({data:answer}).then(
              function(methodAnswer) {
                $scope.vm.deviceObservers = _.filter($scope.vm.deviceObservers,function(item) {
                  return item.deviceId != answer.deviceId;
                });
                $scope.vm.deviceObservers.push(answer);
              },
              function(methodAnswer) {

              }
            );
          }, function() {

          }
        );
    };

    $scope.doDeleteObserver = function(ev, observer) {
      DevMonitorService.deleteDevice({data:observer}).then(
        function(methodAnswer) {
          $scope.vm.deviceObservers = _.filter($scope.vm.deviceObservers,function(item) {
            return item.deviceId != observer.deviceId;
          });
        },
        function(methodAnswer) {

        }
      );
    };

  });
