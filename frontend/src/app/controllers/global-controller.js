'use strict';

/**
 * Created by omist on 04.12.2016.
 */
angular.module('syClientangularApp')
  .controller('DeviceObserverDialogController', function ($scope, settings, $mdDialog, platforms, observer) {

    $scope.editing = observer;

    $scope.deviceId = undefined;
    $scope.ipAddress = '';
    $scope.description = '';
    $scope.mac = '';
    $scope.platform = '';

    if (observer) {
      $scope.deviceId = observer.deviceId;
      $scope.ipAddress = observer.ipAddress;
      $scope.description = observer.description;
      $scope.mac = observer.mac;
      $scope.platform = observer.type;
    }

    $scope.platforms = platforms;

    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    $scope.save = function() {
      var devObserver = {
        "description": $scope.description,
        "deviceId": $scope.deviceId,
        "ipAddress": $scope.ipAddress,
        "mac": $scope.mac,
        "type": $scope.platform
      };
      $mdDialog.hide(devObserver);
    };

  });

angular.module('syClientangularApp')
  .controller('GlobalController', function ($scope, settings, $mdDialog) {
    $scope.appName = settings.appName;




  });

angular.module('syClientangularApp')
  .filter('toDateString', function() {
    return function(ts) {
      if (ts) {
        var mo = moment.unix(ts / 1000);
        return mo.format("MM/DD/YYYY HH:mm:ss");
      } else {
        return 'Undefined'
      }
    };
  });
angular.module('syClientangularApp')
  .filter('observerIcon', function(DevMonitorService) {
    return function(id) {
      var r = _.filter(DevMonitorService.arrayPlatforms(), function(item) {return item.id==id})[0];
      if (r) {
        return r.icon;
      } else {
        return 'android';
      }
    };
  });



