'use strict';

/**
 * @ngdoc function
 * @name syClientangularApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the syClientangularApp
 */
angular.module('syClientangularApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
