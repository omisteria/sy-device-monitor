import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from './material.module';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ApiService} from '../services/api.service';
import {SyPlatformComponent} from "../comp/platform/platform.component";
import {SyDeviceDetailsComponent} from "../comp/device-details/device-details.component";
import {SyNavbarService} from "../services/navbar.service";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    MaterialModule
  ],
  declarations: [
    SyPlatformComponent,

  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SyPlatformComponent
  ],
  providers: [
    ApiService,
    SyNavbarService,
    FormBuilder
  ]
})
export class SharedModule { }
