import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs/Rx';
import {Device, DeviceObserver, DevicePage} from '../models/device';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class ApiService {

  private baseURL;

  constructor(private http: HttpClient) {
    this.baseURL = environment.baseURL;
  }

  arrayPlatforms(): Observable<any> {
    return Observable.of([
      {id:'android',name:'Android',icon:'android'},
      {id:'mac',name:'OSX',icon:'desktop_mac'},
      {id:'windows-mobile',name:'Windows Mobile',icon:'windows'},
      {id:'windows',name:'Windows',icon:'desktop_windows'},
      {id:'linux',name:'Linux',icon:'pets'}
    ]);
  }

  getAllDevices(): Observable<DeviceObserver[]> {
    return this.http.get<DeviceObserver[]>(this.baseURL + '/v1/devmonitor/observers/all')
      .catch(this.handleError);
  }

  saveDevice(device: Device): Observable<Device> {
    return this.http.post<Device>(this.baseURL + '/v1/devmonitor/observer', device)
      .catch(this.handleError);
  }

  /*updateDevice(device: Device): Observable<Device> {
    return this.http.put<Device>(this.baseURL + '/v1/devmonitor/observer', device)
      .catch(this.handleError);
  }*/

  deleteDevice(device: Device): Observable<Device> {
    return this.http.put<Device>(this.baseURL + '/v1/devmonitor/observer/'+device.deviceId, device)
      .catch(this.handleError);
  }

  handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
