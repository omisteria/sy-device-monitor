import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/Rx";

@Injectable()
export class SyNavbarService {

  ls_key: string = 'navbar.state';
  state = false;
  stateChange: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.state);

  constructor() {
    const nbv = localStorage.getItem(this.ls_key);
    this.state = nbv === 'open';
    this.stateChange.next(this.state);
  }

  toggle() {
    if (this.state) {
      this.close();
    } else {
      this.open();
    }
  }

  open() {
    this.state = true;
    this.stateChange.next(this.state);
    localStorage.setItem(this.ls_key, 'open');
  }

  close() {
    this.state = false;
    this.stateChange.next(this.state);
    localStorage.setItem(this.ls_key, 'close');
  }
}
