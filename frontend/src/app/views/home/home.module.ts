import {CommonModule} from "@angular/common";
import {SharedModule} from "../../modules/shared.module";
import {FormsModule} from "@angular/forms";
import {LandingPageComponent} from "./landing-page.component";
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";

const routes: Routes = [
  {
    path     : 'home/landing',
    component: LandingPageComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    FormsModule,
    SharedModule
  ],
  declarations: [
    LandingPageComponent
  ],
  providers: [

  ]
})
export class HomeModule { }
