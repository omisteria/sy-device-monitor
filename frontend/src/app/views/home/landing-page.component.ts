import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatSort, MatTableDataSource, Sort} from '@angular/material';
import {Device, DeviceObserver} from "../../models/device";
import {ApiService} from "../../services/api.service";
import {SyDeviceDetailsComponent} from "../../comp/device-details/device-details.component";


@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;

  devices: DeviceObserver[];
  displayedColumns = ['id', 'description', 'ipaddress', 'macaddress', 'lastupdate', 'actions'];
  dataSource: MatTableDataSource<any> = new MatTableDataSource([]);
  sortActive: string = '';
  sortDirection: string = '';

  constructor(
    private apiService: ApiService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.loadData();

    setInterval(() => {
      this.loadData();
    }, 30000);
  }

  private loadData() {
    this.apiService.getAllDevices()
      .subscribe(data => {
        this.devices = data;
        this.sorting();
      });
  }


  addDevice() {
    event.stopPropagation();
    const dialogRef = this.dialog.open(SyDeviceDetailsComponent, {
      width: '70%',
      data: {
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loadData();
      }
    });
  }

  editRow(row: DeviceObserver , event) {
    event.stopPropagation();
    const dialogRef = this.dialog.open(SyDeviceDetailsComponent, {
      width: '70%',
      data: {
        row: row.device
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loadData();
      }
    });
  }

  sortData(sort: Sort) {
    if (!sort.active || sort.direction == '') {
      return;
    }
    this.sortActive = sort.active;
    this.sortDirection = sort.direction;
    this.sorting();
  }

  sorting() {
    this.dataSource.data = this.devices.sort((a, b) => {
      let isAsc = this.sortDirection == 'asc';
      switch (this.sortActive) {
        case 'id': return this.compare(a.device.deviceId, b.device.deviceId, isAsc);
        case 'ipaddress': return this.compare(a.device.ipAddress, b.device.ipAddress, isAsc);
        case 'macaddress': return this.compare(a.device.mac, b.device.mac, isAsc);
        default: return 0;
      }
    });
  }

  compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  click1() {
    /**/
  }
  click2() {
    console.log('click 2');
  }
  click3() {
    console.log('click 3');
  }
}
