package com.synapse.ms.devmonitor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.synapse.ms.devmonitor.intevents.RegistryDeviceAppendedEvent;
import com.synapse.ms.devmonitor.intevents.RegistryDeviceChangeAvailabilityEvent;
import com.synapse.ms.devmonitor.intevents.RegistryDeviceDeletedEvent;
import com.synapse.ms.devmonitor.intevents.RegistryDeviceUpdatedEvent;
//import com.synapse.ms.devmonitor.MqttGateway;
import com.synapse.ms.devmonitor.jms.MsIntegrationService;
import com.synapse.ms.devmonitor.model.Device;
import com.synapse.ms.devmonitor.model.DeviceObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@Component
public class AppEventHandlers {

    private static final Logger logger = LoggerFactory.getLogger(AppEventHandlers.class);

    private MqttGateway mqttGateway;


    @Autowired
    private MsIntegrationService msIntegrationService;

    @Autowired
    public void context(ApplicationContext context) {
        this.mqttGateway = context.getBean(MqttGateway.class);
    }

    @EventListener
    public void handleOrderCreatedEvent(RegistryDeviceAppendedEvent<Device> event) {
        // runObservers();
    }

    @EventListener
    public void handleOrderCreatedEvent(RegistryDeviceUpdatedEvent<Device> event) {
        // runObservers();
    }

    @EventListener
    public void handleOrderCreatedEvent(RegistryDeviceDeletedEvent<Device> event) {
        // runObservers();
    }

    @EventListener
    public void handleChangeStateEvent(RegistryDeviceChangeAvailabilityEvent<Device> event) {

        DeviceObserver deviceObserver = (DeviceObserver)event.getSource();

        Map<String, String> messageMap = new HashMap();
        messageMap.put("sender", "devmonitor");
        messageMap.put("action", "state");
        messageMap.put("deviceid", deviceObserver.getDevice().getDeviceId());
        messageMap.put("state", String.valueOf(event.getValue()));

        // Send message
        try {
            String payload = new ObjectMapper().writeValueAsString(messageMap);
            mqttGateway.sendDeviceStatus(MessageBuilder.withPayload(payload).build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
