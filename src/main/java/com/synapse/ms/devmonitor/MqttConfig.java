package com.synapse.ms.devmonitor;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.stereotype.Component;

@Component
public class MqttConfig {

    @Value("${spring.server.mqtt.url}")
    private String mqttUrl;

    @Value("${spring.server.mqtt.client}")
    private String clientName;

    @Value("${spring.server.mqtt.topic}")
    private String mainTopic;

    @Value("${spring.server.mqtt.qos:2}")
    private Integer mqttQos;


    @Bean
    public MessageChannel mqttOutboundChannel() {
        return new DirectChannel();
    }

    @Bean
    public MqttPahoClientFactory mqttClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        MqttConnectOptions options = new MqttConnectOptions();
        options.setServerURIs(new String[] { mqttUrl });
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        //options.setUserName("username");
        //options.setPassword("password".toCharArray());
        factory.setConnectionOptions(options);
        return factory;
    }

    @Bean
    @ServiceActivator(inputChannel = "mqttOutboundChannel")
    public MessageHandler mqttOutbound() {
        MqttPahoMessageHandler messageHandler =
                new MqttPahoMessageHandler(clientName, mqttClientFactory());

        // The caller will not block messages waiting for a delivery confirmation when
        // a message is sent so that the message can be received. Default false
        messageHandler.setAsync(true);
        messageHandler.setDefaultTopic(mainTopic);
        messageHandler.setDefaultQos(mqttQos);
        messageHandler.setCompletionTimeout(50000);
        return messageHandler;
    }
}
