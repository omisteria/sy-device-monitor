package com.synapse.ms.devmonitor;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;


@MessagingGateway()
public interface MqttGateway {

    //void send(@Header(MqttHeaders.TOPIC) String topic, String data);

    @Gateway(requestChannel = "mqttOutboundChannel")
    void sendDeviceStatus(Message message);
}
