package com.synapse.ms.devmonitor.exception;

/**
 * Created by omist on 20.11.2016.
 */
public class ObserverNotFoundException extends EntityNotFoundException {

    public ObserverNotFoundException() {
        super();
    }

    public ObserverNotFoundException(String message) {
        super(message);
    }

    public ObserverNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ObserverNotFoundException(Throwable cause) {
        super(cause);
    }

    protected ObserverNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
