package com.synapse.ms.devmonitor.intevents;

import org.springframework.context.ApplicationEvent;

/**
 * Created by omist on 21.11.2016.
 */
public class RegistryDeviceAppendedEvent<T> extends ApplicationEvent {

    public RegistryDeviceAppendedEvent(T source) {
        super(source);
    }
}
