package com.synapse.ms.devmonitor.intevents;

import org.springframework.context.ApplicationEvent;

/**
 * Created by omist on 27.11.2016.
 */
public class RegistryDeviceChangeAvailabilityEvent<T> extends ApplicationEvent {

    private boolean value;

    public RegistryDeviceChangeAvailabilityEvent(Object source, boolean value) {
        super(source);
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }
}
