package com.synapse.ms.devmonitor.intevents;

import org.springframework.context.ApplicationEvent;

/**
 * Created by omist on 21.11.2016.
 */
public class RegistryDeviceDeletedEvent<T> extends ApplicationEvent {

    public RegistryDeviceDeletedEvent(T source) {
        super(source);
    }
}