package com.synapse.ms.devmonitor.intevents;

import org.springframework.context.ApplicationEvent;

/**
 * Created by omist on 21.11.2016.
 */
public class RegistryDeviceUpdatedEvent<T> extends ApplicationEvent {

    public RegistryDeviceUpdatedEvent(T source) {
        super(source);
    }
}
