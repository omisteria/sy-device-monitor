package com.synapse.ms.devmonitor.jms;

/**
 * Created by omist on 16.04.2017.
 */
public class BaseMessage {

    private String sender;
    private String ident;
    private String value;

    public BaseMessage() {
    }

    public BaseMessage(String sender, String ident, String value) {
        this.sender = sender;
        this.ident = ident;
        this.value = value;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BaseMessage{" +
                "sender='" + sender + '\'' +
                ", ident='" + ident + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
