package com.synapse.ms.devmonitor.jms;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by omist on 12.04.2017.
 */
@Component
public class MessageReceiver {

    public void onMessage(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        BaseMessage bm = null;

        try {
            bm = mapper.readValue((byte[])object, BaseMessage.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(bm);
    }
}
