package com.synapse.ms.devmonitor.jms;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;


/**
 * Created by omist on 21.11.2016.
 */
@Component
@ComponentScan(basePackages = "com.synapse.ms.devmonitor")
public class MsIntegrationService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    final static String topicName = "synapse.exchange";
    final static String queueNameHistory = "history";

    @Bean
    public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        RetryTemplate retryTemplate = new RetryTemplate();
        ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setInitialInterval(500);
        backOffPolicy.setMultiplier(10.0);
        backOffPolicy.setMaxInterval(10000);
        retryTemplate.setBackOffPolicy(backOffPolicy);
        template.setRetryTemplate(retryTemplate);
        return template;
    }

/*    @Bean
    public ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory("192.168.3.10");
    }*/

/*    @Bean
    public AmqpAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        return new RabbitTemplate(connectionFactory());
    }*/

    //@Autowired
    /*public MsIntegrationService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }*/

    @Bean
    public Queue queueHistory() {
        return new Queue(queueNameHistory, false);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(topicName);
    }

    @Bean
    public Binding binding(Queue queueHistory, TopicExchange exchange) {
        return BindingBuilder.bind(queueHistory).to(exchange).with("devmonitor.device.#");
    }

    /*@Bean
    public MessageConverter jsonMessageConverter(){
        return new JsonMessageConverter();
    }*/

    /*@Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueNameHistory);
        container.setMessageConverter(jsonMessageConverter());
        container.setMessageListener(listenerAdapter);

        return container;
    }*/

    /*@Bean
    MessageListenerAdapter listenerAdapter(MessageReceiver messageReceiver) {
        return new MessageListenerAdapter(messageReceiver, "onMessage");
    }*/

    public void sendMessage(Object message) throws Exception {
        rabbitTemplate.convertAndSend(topicName, "devmonitor.device.availability", message);
    }

    /*@Bean
    public JmsListenerContainerFactory<?> myFactory(ConnectionFactory connectionFactory,
                                                    DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        // You could still override some of Boot's default if necessary.
        return factory;
    }

    @Bean // Serialize message content to json using TextMessage
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }*/
}
