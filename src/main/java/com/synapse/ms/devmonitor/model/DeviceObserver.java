package com.synapse.ms.devmonitor.model;

public class DeviceObserver {

    private Device device;

    private boolean available;

    private long timeStatusChange;


    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public long getTimeStatusChange() {
        return timeStatusChange;
    }

    public void setTimeStatusChange(long timeStatusChange) {
        this.timeStatusChange = timeStatusChange;
    }
}
