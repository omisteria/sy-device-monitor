package com.synapse.ms.devmonitor.repositories;

import com.synapse.ms.devmonitor.model.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends JpaRepository<Device, String> {

    //Page<DeviceObserver> fin
}
