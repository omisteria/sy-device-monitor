package com.synapse.ms.devmonitor.services;

import com.synapse.ms.devmonitor.model.DeviceObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * Mobile devices are getting sleep mode periodically.
 *
 * Created by omist on 21.11.2016.
 */
@Component
public class DeviceMonitor {



    @Autowired
    private DeviceObserversRegistry deviceObserversRegistry;

    ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(10);

    @Scheduled(fixedRate = 30000)
    public void check() {

        deviceObserversRegistry.loadDevices();

        deviceObserversRegistry.getDeviceObservers().forEach(deviceObserver -> {
            new Thread(() -> {

                PingCheckJob job = new PingCheckJob(deviceObserver, deviceObserversRegistry);
                //Future<DeviceObserver> future =
                newFixedThreadPool.submit(new PingCheckJobManager(25, TimeUnit.SECONDS, job));

            }).start();
        });
    }
}
