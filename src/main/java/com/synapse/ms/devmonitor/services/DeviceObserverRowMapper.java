package com.synapse.ms.devmonitor.services;

import com.synapse.ms.devmonitor.model.Device;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by omist on 20.11.2016.
 */
public class DeviceObserverRowMapper implements RowMapper
{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {

        Device item = new Device();
        item.setDeviceId(rs.getString("device_id"));
        item.setIpAddress(rs.getString("address"));
        item.setDescription(rs.getString("description"));
        item.setMac(rs.getString("mac"));
        item.setType(rs.getString("platform"));

        return item;
    }

}