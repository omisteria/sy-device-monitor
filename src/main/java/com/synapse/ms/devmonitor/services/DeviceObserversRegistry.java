package com.synapse.ms.devmonitor.services;

import com.synapse.ms.devmonitor.intevents.RegistryDeviceAppendedEvent;
import com.synapse.ms.devmonitor.intevents.RegistryDeviceChangeAvailabilityEvent;
import com.synapse.ms.devmonitor.intevents.RegistryDeviceDeletedEvent;
import com.synapse.ms.devmonitor.intevents.RegistryDeviceUpdatedEvent;
import com.synapse.ms.devmonitor.jms.BaseMessage;
import com.synapse.ms.devmonitor.jms.MsIntegrationService;
import com.synapse.ms.devmonitor.model.Device;
import com.synapse.ms.devmonitor.model.DeviceObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by omist on 27.11.2016.
 */
@Component
public class DeviceObserversRegistry {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private ApplicationEventPublisher publisher;

    private Map<String, DeviceObserver> deviceObservers = new ConcurrentHashMap<>();

    private boolean availableMaster;

    public boolean isAvailableMaster() {
        return availableMaster;
    }

    public void setAvailableMaster(boolean availableMaster) {
        this.availableMaster = availableMaster;
    }

    @PostConstruct
    public void init() {
        // periodically refresh
        // apply checking of network status
        loadDevices();
    }

    public void loadDevices() {
        List<Device> devices = deviceService.findAllDevices();
        devices.forEach(device -> {
            DeviceObserver obs = deviceObservers.get(device.getDeviceId());
            if (obs == null) {
                obs = new DeviceObserver();
                obs.setDevice(device);
                deviceObservers.put(device.getDeviceId(), obs);
            } else {
                obs.setDevice(device);
            }
        });
    }

    public Collection<DeviceObserver> getDeviceObservers() {
        return deviceObservers.values();
    }


    public DeviceObserver getDeviceObserver(String deviceId) {
        Assert.notNull(deviceId, "Device ID cannot be null");
        return deviceObservers.get(deviceId);
    }

    /*public Device createDeviceObserver(Device device) {
        Assert.notNull(device, "Device cannot be null");

        deviceService.saveDevice(device);

        DeviceObserver obs = new DeviceObserver();
        obs.setDevice(device);
        deviceObservers.put(device.getDeviceId(), obs);

        publisher.publishEvent(new RegistryDeviceAppendedEvent(device));

        return device;
    }*/

    public Device saveOrUpdateDevice(Device device) {
        Assert.notNull(device, "Device cannot be null");

        Device regDevice = deviceService.getDevice(device.getDeviceId());

        if (regDevice == null) {
            deviceService.saveDevice(device);
            regDevice = device;

            DeviceObserver obs = new DeviceObserver();
            obs.setDevice(device);
            deviceObservers.put(device.getDeviceId(), obs);

            publisher.publishEvent(new RegistryDeviceAppendedEvent(device));
        } else {
            regDevice.setDescription(device.getDescription());
            regDevice.setIpAddress(device.getIpAddress());
            regDevice.setMac(device.getMac());
            regDevice.setType(device.getType());

            deviceService.saveDevice(regDevice);
            deviceObservers.get(regDevice.getDeviceId()).setDevice(regDevice);

            publisher.publishEvent(new RegistryDeviceUpdatedEvent(regDevice));
        }

        return regDevice;
    }

    public Device deleteDevice(String deviceId) {
        Assert.notNull(deviceId, "deviceId cannot be null");

        Device regDevice = deviceService.getDevice(deviceId);
        Assert.notNull(regDevice, "Device not exists");

        deviceService.deleteDevice(regDevice);
        deviceObservers.remove(regDevice.getDeviceId());

        publisher.publishEvent(new RegistryDeviceDeletedEvent(regDevice));

        return regDevice;
    }

    public void setAvailableDevice(String deviceId, boolean value) {
        Assert.notNull(deviceId, "deviceId cannot be null");

        DeviceObserver deviceObserver = deviceObservers.get(deviceId);
        Assert.notNull(deviceObserver, "DeviceObserver not exists");

        //if (deviceObserver.isAvailable() != value) {
            deviceObserver.setAvailable(value);
            deviceObserver.setTimeStatusChange(new Date().getTime());

            publisher.publishEvent(new RegistryDeviceChangeAvailabilityEvent(deviceObserver, value));
        //}
    }
}
