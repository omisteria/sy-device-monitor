package com.synapse.ms.devmonitor.services;

import com.synapse.ms.devmonitor.model.Device;
import com.synapse.ms.devmonitor.repositories.DeviceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by omist on 20.11.2016.
 */
@Service
@Transactional
public class DeviceService {

    @Autowired
    private DeviceRepository deviceRepository;

    private final static Logger logger = LoggerFactory.getLogger(DeviceService.class);

    public Device getDevice(String deviceId) {
        return deviceRepository.findById(deviceId).get();
    }

    public Device saveDevice(Device device) {
        deviceRepository.save(device);
        return device;
    }

    public Device deleteDevice(Device device) {
        deviceRepository.delete(device);
        return device;
    }

    public List<Device> findAllDevices() {
        return deviceRepository.findAll();
    }
}
