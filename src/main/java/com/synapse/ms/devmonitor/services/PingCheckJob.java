package com.synapse.ms.devmonitor.services;

import com.synapse.ms.devmonitor.model.Device;
import com.synapse.ms.devmonitor.model.DeviceObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

public class PingCheckJob implements Callable<DeviceObserver> {

    private static final Logger logger = LoggerFactory.getLogger(PingCheckJob.class);

    private DeviceObserver deviceObserver;
    private DeviceObserversRegistry deviceObserversRegistry;

    public PingCheckJob(DeviceObserver deviceObserver, DeviceObserversRegistry deviceObserversRegistry) {
        this.deviceObserver = deviceObserver;
        this.deviceObserversRegistry = deviceObserversRegistry;
    }

    @Override
    public DeviceObserver call() throws Exception {

        int summCounter = 0;
        boolean summResult = false;

        final Device device = deviceObserver.getDevice();
        boolean res = false;

        try
        {
            long t1= System.currentTimeMillis();

            Process p2 = Runtime.getRuntime().exec("ping -c 1 "+device.getIpAddress());
            p2.waitFor();

            t1 = System.currentTimeMillis() - t1;
            logger.info("Time: "+t1);

            res = t1 < 300;

        } catch (Exception e5) {
            e5.printStackTrace();
        }

        //logger.info("Ping "+deviceObserver.getIpAddress() + ": "+res);


        try {

            //summCounter++;
            //summResult = summResult || res;

            //if (summCounter > 2) {

                //logger.info("Ping "+deviceObserver.getIpAddress() + ": "+res);
                if ("master".equalsIgnoreCase(device.getDeviceId())) {

                    deviceObserversRegistry.setAvailableMaster(res);
                    deviceObserversRegistry.setAvailableDevice(device.getDeviceId(), res);

                } else {

                    if (deviceObserversRegistry.isAvailableMaster()) {
                        deviceObserversRegistry.setAvailableDevice(device.getDeviceId(), res);
                    }
                }

                // summResult = false;
                // summCounter = 0;
            //}

        } catch (Exception e) {
            e.printStackTrace();
        }

        return deviceObserver;
    }
}
