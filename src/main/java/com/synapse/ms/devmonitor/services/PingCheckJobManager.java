package com.synapse.ms.devmonitor.services;

import java.util.concurrent.TimeoutException;
import com.synapse.ms.devmonitor.model.DeviceObserver;
import java.util.concurrent.ExecutionException;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PingCheckJobManager implements Callable<DeviceObserver> {

    protected long timeout;
    protected TimeUnit timeUnit;
    protected Callable<DeviceObserver> job;

    public PingCheckJobManager(long timeout, TimeUnit timeUnit, Callable<DeviceObserver> job) {
        this.timeout = timeout;
        this.timeUnit = timeUnit;
        this.job = job;
    }

    @Override
    public DeviceObserver call() {
        DeviceObserver result = null;
        ExecutorService exec = Executors.newSingleThreadExecutor();

        try {
            result = exec.submit(job).get(timeout, timeUnit);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            // Whatever you want
            if (e instanceof TimeoutException) {
                System.out.println("Timeout get for " + job.toString());
            } else {
                System.out.println("exception get for " + job.toString() + " : " + e.getMessage());
            }
        }
        exec.shutdown();
        return result;
    }
}
