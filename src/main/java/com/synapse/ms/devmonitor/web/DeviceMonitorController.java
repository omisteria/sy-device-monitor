/*
 * Copyright (c) 2016, WSO2 Inc. (http://wso2.com) All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.synapse.ms.devmonitor.web;

import com.synapse.ms.devmonitor.model.Device;
import com.synapse.ms.devmonitor.model.DeviceObserver;
import com.synapse.ms.devmonitor.services.DeviceObserversRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/api")
public class DeviceMonitorController {

    private static final Logger logger = LoggerFactory.getLogger(DeviceMonitorController.class);

    @Autowired
    private DeviceObserversRegistry deviceObserversRegistry;


    @RequestMapping(value = "/observer/{deviceId}", method = RequestMethod.GET)
    @ResponseBody
    public DeviceObserver getDeviceObserver(@PathVariable String deviceId) {
        return deviceObserversRegistry.getDeviceObserver(deviceId);
    }

    @RequestMapping(value = "/observer", method = RequestMethod.POST)
    @ResponseBody
    public Device createDevice(@RequestBody Device device) {
        return deviceObserversRegistry.saveOrUpdateDevice(device);
    }

    /*@RequestMapping(value = "/observer", method = RequestMethod.PUT)
    @ResponseBody
    public Device updateDeviceObserver(@RequestBody Device device) {
        return deviceObserversRegistry.saveOrUpdateDevice(device);
    }*/

    @RequestMapping(value = "/observer/{deviceId}", method = RequestMethod.DELETE)
    @ResponseBody
    public Device deleteDeviceObserver(@PathVariable String deviceId) {
        return deviceObserversRegistry.deleteDevice(deviceId);
    }

    /**
     * Retrieves all device observers.
     *
     * @return All observers will be sent to the client as Json
     * according to the Accept header of the request.
     */
    @RequestMapping("/observers/all")
    @ResponseBody
    public Collection<DeviceObserver> getAllDeviceObservers() {
        return deviceObserversRegistry.getDeviceObservers();
    }
}
