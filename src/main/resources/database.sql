CREATE TABLE devices
(
  device_id character varying(50) NOT NULL,
  description character varying(400),
  mac character varying(25),
  address character varying(15) NOT NULL,
  platform character varying(15),
  CONSTRAINT device_id_pk PRIMARY KEY (device_id)
);
ALTER TABLE devices OWNER TO dbuser;